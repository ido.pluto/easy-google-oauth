import { google } from 'googleapis';
import fs from 'fs-extra';
import path from 'path';

const tokens = path.resolve('./tokens'), refresh_tokenPath = path.join(tokens, 'userToken.txt');
const credentials = await fs.readJSON(path.join(tokens, 'credentials.json')); // the app owner account api info
let refresh_token = await fs.readFile(refresh_tokenPath, 'utf8'); // the refresh token of connect user

function openAuth() {
    const { client_secret, client_id, redirect_uris } = credentials.web;
    return new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
}

function updateRefreshToken(token) {
    refresh_token = token;
    return fs.writeFile(refresh_tokenPath, token);
}

export async function connectToGDrive() {
    const oAuth2Client = openAuth();

    const drive = google.drive({ version: 'v3', auth: oAuth2Client });

    oAuth2Client.setCredentials({ refresh_token });
    oAuth2Client.on('tokens', ({ expiry_date, refresh_token }) => {
        setTimeout(oAuth2Client.getAccessToken.bind(oAuth2Client), expiry_date - Date.now() - 5000);

        if (refresh_token)
            updateRefreshToken(refresh_token);
    });

    await oAuth2Client.getAccessToken();
    return drive;
}