import fs from "fs-extra";

export const { closeOnEnd } = await fs.readJSON('./settings.json');
export const { scopes, appAccount, endPoints } = await fs.readJSON('./account.json');

