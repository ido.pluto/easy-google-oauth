import loginPage, { waitSeconds, clickLast } from './basic.js';
import Table from 'cli-table3';


export default async function listAppsPuppeteer(email, password) {
    const { page, close } = await loginPage("https://console.cloud.google.com/", email, password);
    await clickLast(page, '[data-prober="cloud-console-core-functions-project-switcher"]');
    await waitSeconds(3);

    const entriesProjects = await page.evaluate(() => {
        const selectColum = name => [...document.querySelectorAll(`[data-prober="cloud-console-core-functions-project-${name}"]`)].map(x => x.innerText.trim());
        const ids = selectColum('id');

        // pairs from 2 arrays
        return selectColum('name').map((x, i) => [x, ids[i]]);
    });

    return [
        entriesProjects,
        close
    ];
}

export async function printForUser(entriesProjects, close){
    const table = new Table({
        head: ['Project Name', 'Project Id'],
    });

    entriesProjects.forEach(([name, id]) => table.push([name, id]));

    console.log(table.toString());
    await close();
}