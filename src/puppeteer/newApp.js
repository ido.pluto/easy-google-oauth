import loginPage, { waitSeconds, clickLast, findClick } from './basic.js';
import downloadFile from './downloadFile.js';

async function closePopUps(page){
    try {
        await page.evaluate(() => {
            try {
                document.querySelector('mat-dialog-container mat-dialog-actions button').click();
            } catch {};
        });
        await waitSeconds(1);
    } catch {}
}


export default async function newApp(email, password, { appName = 'n-app', scopes = [], endPoints = [], port = 8090 } = {}) {

    const { page, close } = await loginPage("https://console.cloud.google.com/projectcreate", email, password);

    // agree to terms
    const thereISterms = await page.evaluate(() => {
        try {
            document.querySelectorAll('.mdc-label').forEach(x => x.click());
            document.querySelector('.mat-dialog-actions button').click();
            return true;
        } catch {};
    });

    if(thereISterms){
        await waitSeconds(5);
    }

    //create project
    const input = await page.$('cfc-panel-body input');
    await input.click({ clickCount: 3 })
    await input.type(appName);
    await waitSeconds(4);

    const projectId = await page.evaluate(() => {
        try {
            const projectId = document.querySelector('#mat-mdc-hint-2').children[0].innerText;
            return projectId.substring(0, projectId.length - 1);
        } catch {}
    });


    await clickLast(page, 'button[color="primary"]');
    await waitSeconds(5);

    //config project
    await page.goto(`https://console.cloud.google.com/apis/credentials/consent?project=${projectId}`, {waitUntil: 'networkidle2'});
    await waitSeconds(8);

    await closePopUps(page);
    await clickLast(page, '[value="external"]');
    await clickLast(page, 'button[color="primary"]');
    await waitSeconds(4);

    await page.type('[formcontrolname="displayName"]', appName);

    await clickLast(page, '[formcontrolname="supportEmail"]');
    await findClick(page, 'mat-option', email);

    await page.$eval('[formcontrolname="emails"] input', (el, email) => el.value = email, email);
    await page.type('[formcontrolname="emails"] input', ' ');

    //next config - scopes
    await findClick(page, 'button[type="button"]', 'save and continue');
    await waitSeconds(5);

    await findClick(page, 'button[type="button"]', 'add or remove scopes');
    await waitSeconds(3);

    for (const i of scopes) {
        await page.type('[formcontrolname="manuallyAddedScopesControl"]', i);
        await findClick(page, 'button[type="button"]', 'add to table');
        await waitSeconds(1);
    }

    await findClick(page, 'button[type="submit"]', 'update');
    await waitSeconds(2);

    //skip next config
    await findClick(page, 'button[type="button"]', 'save and continue');
    await waitSeconds(2);

    //skip next config
    await findClick(page, 'button[type="button"]', 'save and continue');
    await waitSeconds(1);

    //enable end points
    for (const i of endPoints) {
        await page.goto(`https://console.cloud.google.com/apis/library/${i}?project=${projectId}`, {waitUntil: 'networkidle2'});
        await waitSeconds(2);
        await findClick(page, 'button', 'enable');
        await waitSeconds(5);
    }

    //make as production
    await page.goto(`https://console.cloud.google.com/apis/credentials/consent?project=${projectId}`, {waitUntil: 'networkidle2'});
    await waitSeconds(3);

    await findClick(page, 'button', 'publish app');
    await waitSeconds(2);

    await findClick(page, 'button[color="primary"]', 'confirm');
    await waitSeconds(2);

    //create oAuto client
    await page.goto(`https://console.cloud.google.com/apis/credentials/oauthclient?project=${projectId}`, {waitUntil: 'networkidle2'});
    await waitSeconds(5);

    await clickLast(page, '[formcontrolname="typeControl"]');
    await waitSeconds(.5);

    await findClick(page, 'mat-option', 'web application');
    await waitSeconds(.5);

    await findClick(page, '[formarrayname="postMessageOrigins"] button[type="button"]', 'add uri');
    await waitSeconds(.5);

    await page.$eval('[formarrayname="postMessageOrigins"] [formcontrolname="uri"]', el => el.value = 'http://localhost:');
    await page.type('[formarrayname="postMessageOrigins"] [formcontrolname="uri"]', port.toString());

    await findClick(page, '[formarrayname="redirectUris"] button[type="button"]', 'add uri');
    await waitSeconds(.5);

    await page.$eval('[formarrayname="redirectUris"] [formcontrolname="uri"]', el => el.value = 'http://localhost:');
    await page.type('[formarrayname="redirectUris"] [formcontrolname="uri"]', port.toString());

    await findClick(page, 'button[type="submit"]', 'create');
    await waitSeconds(8);

    //download client
    const credentials = await downloadFile(page, () => {
        return page.$eval('[track-name="oAuthSecretDownloadSuccess"]', e => e.click());
    })

    close();

    return credentials;
}