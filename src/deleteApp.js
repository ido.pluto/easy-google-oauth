import { appAccount } from "./puppeteer/const.js";
import deleteAppPuppeteer from "./puppeteer/deleteApp.js";

export default async function deleteApp(){
    const closeBrowser = await deleteAppPuppeteer(appAccount.email, appAccount.password);
    await closeBrowser();
}