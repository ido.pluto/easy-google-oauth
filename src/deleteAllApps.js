import { appAccount } from "./puppeteer/const.js";
import deleteApp from "./puppeteer/deleteApp.js";
import listAppsPuppeteer from "./puppeteer/listApps.js";

export default async function deleteAllApps(){
    const [apps] = await listAppsPuppeteer(appAccount.email, appAccount.password);

    for(const [_, appId] of apps){
        await deleteApp(appAccount.email, appAccount.password, appId);
    }
}