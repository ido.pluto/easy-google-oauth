# Connect to Google oAuth2 easily
The main use of this project is to control your own google account with google API

## Who to use?

```bash
git close git@gitlab.com:ido.very.much/easy-google-oauth.git

npm i
```

now edit the account.json with your info - 'newAppAccount' and 'connectUserAccount' must be different

```bash
npm start
```

\*all the output will be saved at *tokens* folder

## What can it do?
- It can create a project and app on google cloud and connect the scopes and endpoints you want
- It will connect a new user to your app
